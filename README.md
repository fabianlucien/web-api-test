# Fabian's starter template for React projects

## Get started
- npm install
- npm run dev
- open localhost:8080
- should work

## What this project contains

- Webpack
- Babel
- React DOM
- Styled components