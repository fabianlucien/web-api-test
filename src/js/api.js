// imports
import fetch from 'node-fetch';
var FormData = require('form-data');

// vars
const url = 'https://api.stampix.com/web';

// API GET to validate promocode
export const validatePromocode = (promocode) => {
    fetch(url + '/promocode/' + promocode)
    .then(function(res) {
        return res.json();
    }).then(function(json) {
        console.log(json);
    });
};

// API POST to place user's order
export const postOrder = () => {
    const formData = new FormData();
    const dummyFormData = [
        { key: 'campaign', value: '5a5a1499a6f4e42e8f93d68d'},
        { key: 'promocode', value: 'FAB86799' },
        { key: 'first_name', value: 'fabian-test'},
        { key: 'last_name', value: 'fabian-last-name-test'},
        { key: 'gender', value: 'male'},
        { key: 'email', value: 'fabian@test.com' },
        { key: 'street', value: 'streettest 9000' },
        { key: 'zip_code', value: 9000 },
        { key: 'city', value: 'Gent' },
        { key: 'country', value: 'NLD' }
    ];
  
    // Populating the formdata object
    dummyFormData.forEach(element => {
      formData.append(element.key, element.value);
    });
  
    // Log the populated object
    console.log(...formData);
  
    // Do POST 
    fetch(url + '/order', {
        method: 'POST',
        body:  formData,
     }).then(res => res.json())
       .then(json => {
        console.log(json);
    });
};