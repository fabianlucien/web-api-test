// Basic stuff we need
import React, { Component } from 'react';
import { render } from 'react-dom';
import styled from 'styled-components';

// import API
import { validatePromocode, postOrder } from './api'

// import other testing components
import CropComponent from '../js/CropComponent';

// images to test with 
import image1 from '../images/dog1.jpg';
import image2 from '../images/dog2.jpg';
import image3 from '../images/dog3.jpg';
import image4 from '../images/dog4.jpg';
import image5 from '../images/dog5.jpg';
const photos = [image1, image2, image3, image4, image5];

// Styled components
const Container = styled.div`
    display: flex;
    flex-direction: column;
    img, button, div {
        margin: 10px auto;
    }
`;

const Button = styled.button`
    border-radius: 3px;
    padding: 0.25em 1em;
    margin: 0 1em;
    background: transparent;
    color: palevioletred;
    border: 2px solid palevioletred;
`;

// React components

const Photos = photos.map((photo, index) =>
    <img key={index} src={photos[index]} width="100px" />
)

// Main Component for now, just for testing.
export default class Main extends Component {

    componentDidMount() {
        // console.log(photos[1])
    }

    render() {
        return (
            <Container>
                <Button onClick={() => { validatePromocode('FAB86799'); }}>GET INFO code</Button>
                <button onClick={() => { postOrder(); }}>POST ORDER</button>
                {/* {Photos} */}
                {/* <CropComponent/> */}
            </Container>
        );
    }
}

render(<Main />, document.getElementById('app'));