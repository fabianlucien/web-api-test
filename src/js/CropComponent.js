import React, { Component } from 'react';
import styled from 'styled-components';
import Cropper from 'cropperjs'

// image import
import image1 from '../images/dog1.jpg';

const Container = styled.div`
    width: 500px;
    img {
        max-width: 100%;
    }
`;

const ImageContainer = styled.div`
    width: 500px;
`

const Image = styled.img`
    max-width: 100%; /* This has always be set on 100% */
`

const ImageTest = () => {
    return (
        <div>
            <img src={image1} id="image" width="500px" />
        </div>
    );
};

class CropComponent extends React.Component {

    componentDidMount() {
        // var image = document.getElementById('image');
        // var cropper = new Cropper(image, {
        //     aspectRatio: 16 / 9,
        //     crop: function(e) {
        //     console.log(e.detail.x);
        //     console.log(e.detail.y);
        //     console.log(e.detail.width);
        //     console.log(e.detail.height);
        //     console.log(e.detail.rotate);
        //     console.log(e.detail.scaleX);
        //     console.log(e.detail.scaleY);
        //     }
        // });
    }
    
    render() {
      return (
        <div>
            <h1>Crop component</h1>
            <ImageContainer>
                <ImageTest />
            </ImageContainer>
      
        </div>
      );
    }
}

export default CropComponent;